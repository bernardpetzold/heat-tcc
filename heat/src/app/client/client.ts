export class Client {
    id: string;
    name: string;
    contact: string;
    phone: string;
    email: string;
    constructor() {
        this.name = '';
        this.contact = '';
    }
}
