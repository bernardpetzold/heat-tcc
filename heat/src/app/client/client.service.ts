import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Client } from './client';
import { GatewayService } from '../gateway.service';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  endpoint = 'customer';
  page = 1;
  filter = '';
  order = '';
  
  constructor(private gateway: GatewayService) { }

  getClient() {
    return this.gateway.get(`${this.endpoint}?page=${this.page}&${this.filter}&${this.order}`);
  }

  getMoreClient() {
    this.page++;
    return this.gateway.get(`${this.endpoint}?page=${this.page}&${this.filter}&${this.order}`)
  }

  getAllClient() {
    return this.gateway.get(`${this.endpoint}/all`)
  }

  postClient(client: Client) {
    return this.gateway.post(this.endpoint, client);
  }

  editClient(client: Client) {
    return this.gateway.put(this.endpoint, client.id, client);
  }

  setFilter(column, filter) {
    this.filter = `column=${column}&filter=${filter}`;
    this.page = 1;
  }

  setSort(column, order) {
    this.order = `sortBy=${column}&order=${order}`;
    this.page = 1;
  }
}
