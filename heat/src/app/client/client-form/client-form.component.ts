import { Component, OnInit } from '@angular/core';
import { Client } from '../client';
import { ClientService } from '../client.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.css']
})
export class ClientFormComponent implements OnInit {
  emails: any = [{
    name: '',
    warn: '',
  }];
  phones: any = [{
    name: '',
    warn: '',
  }];
  client: Client = new Client();
  constructor(private service: ClientService, private route: Router) { }

  ngOnInit() {
  }

  trackByIndex(index: number, obj: any): any {
    return index;
  }

  addEmail() {
    this.emails.push([{name: '', warn:''}]);
  }

  removeEmail() {
    if (this.emails.length > 1) {
      this.emails.splice(this.emails.length - 1, 1);
    }
  }

  addPhone() {
    this.phones.push([{name: '', warn: ''}]);
  }

  removePhone() {
    if (this.phones.length > 1) {
      this.phones.splice(this.phones.length - 1, 1);
    }
  }

  concatEmail() {
    this.client.email = '';
    this.emails.forEach(element => {
      this.client.email = `${element.name}; ${this.client.email}`
    });
  }

  concatPhone() {
    this.client.phone = '';
    this.phones.forEach(element => {
      this.client.phone = `${element.name}; ${this.client.phone}`
    });
  }
  
  validateName(name: string) {
    if (name.length >= 3) {
      return false;
    } else {
      return true;
    }
  }
  
  validatePhone(phone: string) {
    try{
      if (phone.length >= 8) {
        return false;
      } else {
        return true;
      }
    }catch{
      return true;
    }
  }
  
  validateEmail(email: string) {
    try{
    if (email.length >= 10 && email.includes('@') && email.includes('.')) {
      return false;
    } else {
      return true;
    }
  }catch{
    return false;
  }
  }
  
    validator(client: Client) {
      let response: boolean = false;
      if (!this.validateName(client.name) && !this.validateName(client.contact)){
        for (let i=0; i<this.emails.length; i++){
          if (this.validateEmail(this.emails[i].name)){
            return response;
          }
        }
        for (let i=0; i<this.phones.length; i++){
          if (this.validatePhone(this.phones[i].name)){
            return response;
          }
        }  
      } else {
        return response
      }
      response = true;
      return response;
    }
  
  send(client: Client) {
    if (this.validator(client)) {
      this.concatEmail();
      this.concatPhone();
      this.service.postClient(client).subscribe(res => {
        this.route.navigate([('/filtrarclientes')]);
        return alert('Cliente salvo com sucesso!')
      }, err => {
        console.log(err);
        alert('Ops! Algo deu errado, por favor tente novamente.');
      });
    } else {
      return alert('Por favor, preencha todos os campos corretamente');
    }
  }
}
