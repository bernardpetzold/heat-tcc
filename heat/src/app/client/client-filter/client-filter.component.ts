import { Component, OnInit, TemplateRef } from '@angular/core';
import { ClientService } from '../client.service';
import { Client } from '../client';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

@Component({
  selector: 'app-client-filter',
  templateUrl: './client-filter.component.html',
  styleUrls: ['./client-filter.component.css']
})
export class ClientFilterComponent implements OnInit {
  clients: Client[];
  modalRef: BsModalRef;
  search: any = { filter: '', column: '0' };
  sort: any = { order: null, column: null };
  emails: any = [{
    name: '',
    warn: '',
  }];
  phones: any = [{
    name: '',
    warn: '',
  }];
  client: Client = new Client();
  more: boolean;

  constructor(private service: ClientService, private route: Router, private modalService: BsModalService) { }

  ngOnInit() {
    this.service.page = 1;
    this.loadClients();
  }

  loadClients() {
    this.service.getClient().subscribe((res: Client[]) => {
      this.clients = res;
      this.more = this.service.page * 10 != this.clients.length;
    });
  }

  getMoreCustomers() {
    this.service.getMoreClient().subscribe((res: Client[]) => {
      res.forEach(element => {
        this.clients.push(element);
      });
    });
    this.more = this.service.page * 10 != this.clients.length;
  }

  sortTable(sort) {
    this.sort.column = sort;
    if (this.sort.order == 'asc') {
      this.sort.order = 'desc';
    } else {
    this.sort.order = 'asc';      
    }
    this.service.setSort(this.sort.column, this.sort.order);
    this.loadClients();
  }

  setFilter() {
    this.service.setFilter(this.search.column, this.search.filter);
    this.loadClients();
  }

  trackByIndex(index: number, obj: any): any {
    return index;
  }

  addEmail() {
    this.emails.push([{ name: '', warn: '' }]);
  }

  removeEmail() {
    if (this.emails.length > 1) {
      this.emails.splice(this.emails.length - 1, 1);
    }
  }

  addPhone() {
    this.phones.push([{ name: '', warn: '' }]);
  }

  removePhone() {
    if (this.phones.length > 1) {
      this.phones.splice(this.phones.length - 1, 1);
    }
  }

  desmontEmail() {
    this.emails = [];
    let emailsTemp
    emailsTemp = this.client.email.split('; ');
    for (let i = 0; i < emailsTemp.length - 1; i++) {
      this.emails.push({ name: '', warn: '' });
      this.emails[i].name = emailsTemp[i];
    }
  }

  desmontPhone() {
    this.phones = [];
    let phonesTemp;
    phonesTemp = this.client.phone.split('; ');
    for (let i = 0; i < phonesTemp.length - 1; i++) {
      this.phones.push({ name: '', warn: '' });
      this.phones[i].name = phonesTemp[i];
    }
  }

  concatEmail() {
    this.client.email = '';
    this.emails.forEach(element => {
      this.client.email = `${element.name}; ${this.client.email}`
    });
  }

  concatPhone() {
    this.client.phone = '';
    this.phones.forEach(element => {
      this.client.phone = `${element.name}; ${this.client.phone}`
    });
  }

  validateName(name: string) {
    if (name.length >= 3) {
      return false;
    } else {
      return true;
    }
  }

  validatePhone(phone: string) {
    try {
      if (phone.length >= 8) {
        return false;
      } else {
        return true;
      }
    } catch{
      return true;
    }
  }

  validateEmail(email: string) {
    try {
      if (email.length >= 10 && email.includes('@') && email.includes('.')) {
        return false;
      } else {
        return true;
      }
    } catch{
      return true;
    }
  }

  validator(client: Client) {
    let response: boolean = false;
    if (!this.validateName(client.name) && !this.validateName(client.contact)) {
      for (let i = 0; i < this.emails.length; i++) {
        if (this.validateEmail(this.emails[i].name)) {
          return response;
        }
      }
      for (let i = 0; i < this.phones.length; i++) {
        if (this.validatePhone(this.phones[i].name)) {
          return response;
        }
      }
    } else {
      return response
    }
    response = true;
    return response;
  }

  send(client: Client) {
    if (this.validator(client)) {
      this.concatEmail();
      this.concatPhone();
      this.service.editClient(client).subscribe(res => {
        this.route.navigate([('/filtrarclientes')]);
        this.modalRef.hide();
        alert('Cliente salvo com sucesso!')
      }, err => {
        console.log(err);
        alert(err.statusText);
        this.modalRef.hide();
      });
    } else {
      return alert('Por favor, preencha todos os campos corretamente');
    }
  }

  openEditModal(client, template: TemplateRef<any>) {
    this.client = client;
    this.desmontEmail();
    this.desmontPhone();
    this.modalRef = this.modalService.show(template);
  }

}
