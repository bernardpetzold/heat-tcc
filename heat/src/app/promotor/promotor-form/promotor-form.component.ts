import { Component, OnInit } from '@angular/core';
import { PromotorService } from 'src/app/promotor/promotor.service';
import { Promotor } from '../promotor';
import { Client } from '../../client/client';
import { Router } from '@angular/router';
import { PromotorFilterComponent } from '../promotor-filter/promotor-filter.component';
import { Acao } from '../../eventos/acao';

@Component({
  selector: 'app-promotor-form',
  templateUrl: './promotor-form.component.html',
  styleUrls: ['./promotor-form.component.css']
})
export class PromotorFormComponent implements OnInit {
  phones: any = [{
    name: '',
    warn: '',
  }];
  promo: Promotor = new Promotor();
  constructor(private service: PromotorService, private route: Router) { }

  ngOnInit() {
  }

  addPhone() {
    this.phones.push([new Promotor()]);
  }

  removePhone() {
    if (this.phones.length > 1) {
      this.phones.splice(this.phones.length - 1, 1);
    }
  }

  concatPhone() {
    this.promo.phone = '';
    this.phones.forEach(element => {
      this.promo.phone = `${element.name}; ${this.promo.phone}`
    });
  }


  validateName(name: string) {
    if (name.length >= 3) {
      return false;
    } else {
      return true;
    }
  }

  validatePhone(phone: string) {
    try{
      if (phone.length >= 8) {
        return false;
      } else {
        return true;
      }
    } catch{
      return true;
    }
  }

  validateEmail(email: string) {
    try{
      if (email.length >= 10 && email.includes('@') && email.includes('.')) {
      return false;
    } else {
      return true;
      }
    } catch{
      return true;
    }
  }

  validator(promo) {
    let response: boolean = false;
    if (!this.validateName(promo.name) && !this.validateEmail(promo.email)) {
      for (let i = 0; i < this.phones.length; i++) {
        if (this.validatePhone(this.phones[i].name)) {
          return response;
        }
      }
    } else {
      return response
    }
    response = true;
    return response;
  }

  send(promo: Promotor) {
    if (this.validator(promo)) {
      this.concatPhone();
      this.service.postPromotor(promo).subscribe(res => {
        this.route.navigate([('/filtrarpromotores')]);
        return alert('Parceiro salvo com sucesso!')
      },
    err => {
      console.log(err);
      alert('Ops! Algo deu errado, por favor tente novamente.');
    })
    } else {
      return alert('Por favor, preencha todos os campos corretamente');
    }
  }
}
