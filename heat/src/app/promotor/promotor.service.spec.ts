import { TestBed } from '@angular/core/testing';

import { PromotorService } from './promotor.service';

describe('PromotorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PromotorService = TestBed.get(PromotorService);
    expect(service).toBeTruthy();
  });
});
