import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotorFilterComponent } from './promotor-filter.component';

describe('PromotorFilterComponent', () => {
  let component: PromotorFilterComponent;
  let fixture: ComponentFixture<PromotorFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotorFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotorFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
