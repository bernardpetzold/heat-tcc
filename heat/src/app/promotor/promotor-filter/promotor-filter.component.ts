import { Component, OnInit, TemplateRef } from '@angular/core';
import { PromotorService } from '../promotor.service';
import { Promotor } from '../promotor';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

@Component({
  selector: 'app-promotor-filter',
  templateUrl: './promotor-filter.component.html',
  styleUrls: ['./promotor-filter.component.css']
})
export class PromotorFilterComponent implements OnInit {
  promos: any;
  modalRef: BsModalRef;
  search: any = '';
  sort: any = '';
  phones: any = [{
    name: '',
    warn: '',
  }];
  promo: Promotor = new Promotor();
  more: boolean;

  constructor(private service: PromotorService, private route: Router, private modalService: BsModalService) { }

  ngOnInit() {
    this.service.page = 1;
    this.loadPromos();
  }

  loadPromos() {
    this.service.getPromos().subscribe(res => {
      this.promos = res;
      this.more = this.service.page * 10 != this.promos.length;
    });
  }

  getMorePromos() {
    this.service.getMorePromos().subscribe((res: Promotor[]) => {
      res.forEach(element => {
        this.promos.push(element);
      });
    });
    this.more = this.service.page * 10 != this.promos.length;
  }

  sortTable(sort) {
    if (this.sort == 'asc') {
      this.sort = 'desc';
    } else {
      this.sort = 'asc';
    }
    this.service.setSort(this.sort);
    this.loadPromos();
  }

  setFilter() {
    this.service.setFilter(this.search);
    this.loadPromos();
  }

  addPhone() {
    this.phones.push([new Promotor()]);
  }

  removePhone() {
    if (this.phones.length > 1) {
      this.phones.splice(this.phones.length - 1, 1);
    }
  }

  concatPhone() {
    this.promo.phone = '';
    this.phones.forEach(element => {
      this.promo.phone = `${element.name}; ${this.promo.phone}`
    });
  }


  validateName(name: string) {
    if (name.length >= 3) {
      return false;
    } else {
      return true;
    }
  }

  desmontPhone() {
    this.phones = [];
    let phonesTemp;
    phonesTemp = this.promo.phone.split('; ');
    for (let i = 0; i < phonesTemp.length - 1; i++) {
      this.phones.push({ name: '', warn: '' });
      this.phones[i].name = phonesTemp[i];
    }
  }

  validatePhone(phone: string) {
    try {
      if (phone.length >= 8) {
        return false;
      } else {
        return true;
      }
    } catch{
      return true;
    }
  }

  validateEmail(email: string) {
    try {
      if (email.length >= 10 && email.includes('@') && email.includes('.')) {
        return false;
      } else {
        return true;
      }
    } catch{
      return true;
    }
  }

  validator(promo) {
    let response: boolean = false;
    if (!this.validateName(promo.name) && !this.validateEmail(promo.email)) {
      for (let i = 0; i < this.phones.length; i++) {
        if (this.validatePhone(this.phones[i].name)) {
          return response;
        }
      }
    } else {
      return response
    }
    response = true;
    return response;
  }

  send(promo: Promotor) {
    if (this.validator(promo)) {
      this.concatPhone();
      this.service.editPromotor(promo).subscribe(() => {
        this.modalRef.hide();
        return alert('Promotor salvo com sucesso!')
      },
        err => {
          alert(err.message);
        })
    } else {
      return alert('Por favor, preencha todos os campos corretamente');
    }
  }

  openEditModal(promo, template: TemplateRef<any>) {
    this.promo = promo;
    this.desmontPhone();
    this.modalRef = this.modalService.show(template);
  }

}
