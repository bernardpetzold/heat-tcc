import { Injectable } from '@angular/core';
import { GatewayService } from '../gateway.service';

@Injectable({
  providedIn: 'root'
})
export class PromotorService {
  endpoint = 'partner';
  page = 1;
  filter = '';
  order = '';
  
  constructor(private gateway: GatewayService) { }

  getPromos() {
    return this.gateway.get(`${this.endpoint}?page=${this.page}&${this.filter}&${this.order}`);
  }

  getMorePromos() {
    this.page++;
    return this.gateway.get(`${this.endpoint}?page=${this.page}&${this.filter}&${this.order}`)
  }

  getAllPromos() {
    return this.gateway.get(`${this.endpoint}/all`)
  }

  postPromotor(promo) {
    return this.gateway.post(this.endpoint, promo);
  }

  editPromotor(promo) {
    return this.gateway.put(this.endpoint, promo.id, promo);
  }
  
  setFilter(filter) {
    this.filter = `column=name&filter=${filter}`;
    this.page = 1;
  }

  setSort(order) {
    this.order = `sortBy=name&order=${order}`;
    this.page = 1;
  }
}
