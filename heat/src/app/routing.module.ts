import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { ClientFormComponent } from './client/client-form/client-form.component';
import { ClientFilterComponent } from './client/client-filter/client-filter.component';
import { PromotorFormComponent } from './promotor/promotor-form/promotor-form.component';
import { PromotorFilterComponent } from './promotor/promotor-filter/promotor-filter.component';
import { EventosFormComponent } from './eventos/eventos-form/eventos-form.component';
import { EventosFilterComponent } from './eventos/eventos-filter/eventos-filter.component';
import { HomeComponent } from './home/home.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'novocliente', component: ClientFormComponent },
  { path: 'filtrarclientes', component: ClientFilterComponent},
  { path: 'novopromotor', component: PromotorFormComponent},
  { path: 'filtrarpromotores', component: PromotorFilterComponent},
  { path: 'novoevento', component: EventosFormComponent},
  { path: 'filtrareventos', component: EventosFilterComponent},

];

@NgModule({
  imports: [ RouterModule.forRoot(appRoutes) ],
  exports: [ RouterModule ]
})
export class RoutingModule { }
