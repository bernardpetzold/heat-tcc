import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ClientFormComponent } from './client/client-form/client-form.component';
import { ClientFilterComponent } from './client/client-filter/client-filter.component';
import { RoutingModule } from './routing.module';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { HeaderComponent } from './header/header.component';
import { EventosFilterComponent } from './eventos/eventos-filter/eventos-filter.component';
import { EventosFormComponent } from './eventos/eventos-form/eventos-form.component';
import { PromotorFilterComponent } from './promotor/promotor-filter/promotor-filter.component';
import { PromotorFormComponent } from './promotor/promotor-form/promotor-form.component';
import { HomeComponent } from './home/home.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';


@NgModule({
  
  declarations: [
    AppComponent,
    ClientFormComponent,
    ClientFilterComponent,
    HeaderComponent,
    EventosFilterComponent,
    EventosFormComponent,
    PromotorFilterComponent,
    PromotorFormComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RoutingModule,
    FormsModule,
    TooltipModule.forRoot(),
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    ModalModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
