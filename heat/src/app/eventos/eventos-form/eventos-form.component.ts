import { Component, OnInit } from '@angular/core';
import { Evento } from '../evento';
import { Acao } from '../acao';
import { EventosService } from '../eventos.service';
import { ClientService } from '../../client/client.service';
import { Client } from '../../client/client';
import { Promotor } from '../../promotor/promotor';
import { PromotorService } from '../../promotor/promotor.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { GatewayService } from '../../gateway.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-eventos-form',
  templateUrl: './eventos-form.component.html',
  styleUrls: ['./eventos-form.component.css']
})
export class EventosFormComponent implements OnInit {
  evento: Evento = new Evento();
  acoes: Acao[] = [new Acao()];
  dateTemp = [];
  clients: Client[];
  promotores: Promotor[];
  bsConfig: Partial<BsDatepickerConfig>;

  constructor(private clientService: ClientService, private promoService: PromotorService, private service: EventosService, private route: Router) { }

  ngOnInit() {
    this.clientService.getAllClient().subscribe((res: Client[]) => {
      this.clients = res;
    });
    this.promoService.getAllPromos().subscribe((res: Promotor[]) => {
      this.promotores = res;
    });
  }

  send(){
    this.evento.Actions = this.acoes;
    this.evento.InitDate = this.dateTemp[0].toDateString();
    this.evento.EndDate = this.dateTemp[1].toDateString();
    this.service.postEvent(this.evento).subscribe(res => {
      alert('Evento criado com sucesso');
      this.route.navigate([('/filtrareventos')]);
    }, err => {
      console.log(err);
      alert(err.message);
    });
  }

  EventDTOFactory() {
    this.evento.Actions = this.acoes;
    console.log(this.evento);
  }

  validateName(name: string) {
    if (name.length >= 3) {
      return false;
    } else {
      return true;
    }
  }

  addAcao() {
    this.acoes.push(new Acao());
  }

  removeAcao() {
    if (this.acoes.length > 1) {
      this.acoes.splice(this.acoes.length - 1, 1);
    }
  }

  addPromo(acao: Acao) {
    acao.Partners.push({Id: '0', Function: '0'});
  }

  removePromo(acao: Acao, i: number) {
    if (acao.Partners.length > 1) {
      acao.Partners.splice(i, 1);
    }
  }

  validator() {
    let response = false;
    if (!this.validateName(this.evento.Name) && this.evento.Customer_Id.length > 2 && this.dateTemp[0] && this.dateTemp) {
      this.acoes.forEach(element => {
        if(this.validateName(element.Name)){
          response = true;
        } else {
          element.Partners.forEach(element2 => {
            if (element2.Id.length < 2 || element2.Function.length < 2) {
              response = true;
            }
          })
        }
      });
      return response;
    } else {
      return true;
    }
  }
}
