import { Component, OnInit, TemplateRef } from '@angular/core';
import { EventosService } from '../eventos.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ClientService } from '../../client/client.service';
import { Router } from '@angular/router';
import { Evento } from '../evento';
import { Acao } from '../acao';
import { Client } from '../../client/client';
import { Promotor } from '../../promotor/promotor';
import { PromotorService } from '../../promotor/promotor.service';

@Component({
  selector: 'app-eventos-filter',
  templateUrl: './eventos-filter.component.html',
  styleUrls: ['./eventos-filter.component.css']
})
export class EventosFilterComponent implements OnInit {
  events: any[];
  evento: Evento = new Evento();
  acoes: Acao[] = [];
  dateTemp = [];
  clients: Client[];
  promotores: Promotor[];
  search: any = { filter: '', column: '0' };
  modalRef: BsModalRef;
  event: any;
  more: boolean;

  constructor(private promoService: PromotorService, private clientService: ClientService, private service: EventosService, private modalService: BsModalService, private route: Router) { }

  ngOnInit() {
    this.service.page = 1;
    this.loadEvents();
    this.clientService.getAllClient().subscribe((res: Client[]) => {
      this.clients = res;
    });
    this.promoService.getAllPromos().subscribe((res: Promotor[]) => {
      this.promotores = res;
    });
  }

  loadEvents() {
    this.service.getEvent().subscribe((res: any[]) => {
      this.events = res;
      this.more = this.service.page * 10 != this.events.length;
      let i=0;
      this.events.forEach(element => {
        element.initDate = new Date(element.initDate).toLocaleDateString();
        element.endDate = new Date(element.endDate).toLocaleDateString();
        i++;
      });
    });
  }

  getMoreEvents() {
    this.service.getMoreEvent().subscribe((res: Evento[]) => {
      res.forEach(element => {
        this.events.push(element);
      });
    });
    this.more = this.service.page * 10 != this.clients.length;
  }

  setFilter() {
    this.service.setFilter(this.search.column, this.search.filter);
    this.loadEvents();
  }


  edit() {
    this.evento.Actions = this.acoes;
    this.evento.InitDate = this.dateTemp[0].toDateString();
    this.evento.EndDate = this.dateTemp[1].toDateString();
    this.service.editEvent(this.evento.Id, this.evento).subscribe(res => {
      alert('Evento atualizado com sucesso');
      window.location.reload();
    }, err => {
      console.log(err);
      alert(err.message);
    });
  }

  EventDTOFactory() {
    this.evento.Actions = this.acoes;
    console.log(this.evento);
  }

  validateName(name: string) {
    if (name.length >= 3) {
      return false;
    } else {
      return true;
    }
  }

  addAcao() {
    this.acoes.push(new Acao());
  }

  removeAcao() {
    if (this.acoes.length > 1) {
      this.acoes.splice(this.acoes.length - 1, 1);
    }
  }

  addPromo(acao: Acao) {
    acao.Partners.push({ Id: '0', Function: '0' });
  }

  removePromo(acao: Acao, i: number) {
    if (acao.Partners.length > 1) {
      acao.Partners.splice(i, 1);
    }
  }

  validator() {
    let response = false;
    if (!this.validateName(this.evento.Name) && this.evento.Customer_Id.length > 2 && this.dateTemp[0] && this.dateTemp) {
      this.acoes.forEach(element => {
        if (this.validateName(element.Name)) {
          response = true;
        } else {
          element.Partners.forEach(element2 => {
            if (element2.Id.length < 2 || element2.Function.length < 2) {
              response = true;
            }
          })
        }
      });
      return response;
    } else {
      return true;
    }
  }


  openEditEventModal(event, template: TemplateRef<any>) {
    this.service.getEventById(event.id).subscribe((res: any) => {
      this.evento.Id = res.id;
      this.evento.Name = res.name;
      this.evento.Customer_Id = res.customer_Id;
      this.evento.isActive = res.isActive;
      this.dateTemp[0] = new Date (res.initDate);
      this.dateTemp[1] = new Date (res.endDate);
      this.acoes = [];
      let i = 0;
      res.actions.forEach(element => {
        this.acoes.push(new Acao);
        this.acoes[i].Id = element.id;
        this.acoes[i].Name = element.name;
        this.acoes[i].Partners = [];
        let i2 = 0;
        element.partners.forEach(element2 => {
          this.addPromo(this.acoes[i]);
          this.acoes[i].Partners[i2].Id = element2.id;
          this.acoes[i].Partners[i2].Function = element2.function;
          i2++;
        });
        i++;
      });
      console.log(res)
      this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'modal-xl' }));
    });
  }

  openEventModal(event, template: TemplateRef<any>) {
    this.service.getEventById(event.id).subscribe((res: any) => {
      this.event = res;
      this.event.initDate = new Date(res.initDate).toLocaleDateString();
      this.event.endDate = new Date(res.endDate).toLocaleDateString();
      this.modalRef = this.modalService.show(template);
    });
  }
}
