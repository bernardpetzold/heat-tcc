import { Injectable } from '@angular/core';
import { Evento } from './evento';
import { Acao } from './acao';
import { GatewayService } from '../gateway.service';

@Injectable({
  providedIn: 'root'
})
export class EventosService {
  endpoint = 'event';
  eventos: Evento[];
  acoes: Acao[];
  page = 1;
  filter = '';

  constructor(private gateway: GatewayService) { }

  getEvent() {
    return this.gateway.get(`${this.endpoint}?page=${this.page}&${this.filter}`);
  }

  getEventById(id) {
    return this.gateway.get(`${this.endpoint}/${id}`);
  }

  getMoreEvent() {
    this.page++;
    return this.gateway.get(`${this.endpoint}?page=${this.page}&${this.filter}}`)
  }

  postEvent (evento: Evento) {
    return this.gateway.post('event', evento);
  }

  editEvent (id, event) {
    return this.gateway.put(this.endpoint, id, event)
  }

  setFilter(column, filter) {
    this.filter = `column=${column}&filter=${filter}`;
    this.page = 1;
  }
}
