import { Client } from '../client/client';
import { Acao } from './acao';

export class Evento {
    Id: string;
    Name: string;
    Customer_Id: string;
    isActive: boolean;
    InitDate: string;
    EndDate: string;
    Actions: Acao[];
    constructor() {
        this.Name = '';
        this.Customer_Id = '0';
        this.isActive = true;
    }
}
