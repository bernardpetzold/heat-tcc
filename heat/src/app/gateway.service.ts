import { Injectable } from '@angular/core';
import { environment } from '../environments/environment.prod';
import { HttpClient, HttpHeaders } from 'node_modules/@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GatewayService {
  url: string = environment.url;
  headers: any;

  constructor(private http: HttpClient) { }

  get(endpoint){
    return this.http.get(`${this.url}${endpoint}`);
  }

  post(endpoint, body){
    return this.http.post(`${this.url}${endpoint}`, body, this.headers);
  }

  put(endpoint, id, body){
    return this.http.put(`${this.url}${endpoint}/${id}`, body, this.headers);
  }

  header() {
    let httpHeader = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    this.headers = {headers: HttpHeaders};
  }
}
