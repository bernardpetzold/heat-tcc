using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Heat.Models
{
  public class Action
  {
    public Action()
    {
      Id = new Guid();
    }

    public Guid Id { get; set; }

    public string Name { get; set; }

    public Guid Event_Id { get; set; }

  }
}
