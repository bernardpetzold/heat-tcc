using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Heat.Models
{
  public class Partner_Action
  {
    public Partner_Action()
    {
      Id = new Guid();
    }

    [Required]
    public Guid Id { get; set; }

    [Required]
    public Guid Partner_Id { get; set; }

    [Required]
    public Guid Action_Id { get; set; }

    public string Function { get; set; }
  }
}
