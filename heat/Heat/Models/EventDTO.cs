using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Heat.Models
{
  public class EventDTO
  {

    public EventDTO ()
    {

    }

    public EventDTO (Event _event, Customer customer, List<ActionDTO> actions)
    {
      Id = _event.Id;
      Name = _event.Name;
      InitDate = _event.InitDate;
      EndDate = _event.EndDate;
      Customer = customer;
      Customer_Id = customer.Id;
      IsActive = _event.IsActive;
      Actions = actions;
    }

    public EventDTO (Event _event, Customer customer)
    {
      Id = _event.Id;
      Name = _event.Name;
      IsActive = _event.IsActive;
      InitDate = _event.InitDate;
      EndDate = _event.EndDate;
      Customer = customer;
      Customer_Id = customer.Id;
    }
    public Guid Id { get; set; }

    public string Name { get; set; }

    public string InitDate { get; set; }

    public string EndDate { get; set; }

    public bool IsActive { get; set; }

    public Customer Customer { get; set; }

    public Guid Customer_Id { get; set; }

    public List<ActionDTO> Actions { get; set; }
  }
}
