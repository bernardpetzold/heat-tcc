using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Heat.Models
{
  public class Customer
  {
    public Customer()
    {
      Id = new Guid();
    }

    public Guid Id { get; set; }

    [Required]
    public string Name { get; set; }

    [Required]
    public string Contact { get; set; }

    public string Email { get; set; }

    public string Phone { get; set; }

  }
}
