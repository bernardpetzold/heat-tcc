using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Heat.Models
{
  public class PartnerDTO
  {

    public PartnerDTO()
    {

    }
    public PartnerDTO(Partner partner, string function)
    {
      Id = partner.Id;
      Name = partner.Name;
      Function = function;
    }

    public Guid Id { get; set; }

    public string Name { get; set; }

    public string Function { get; set; }
  }
}
