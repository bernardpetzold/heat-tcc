using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Heat.Models.ModelConfigurations
{
  public class Partner_ActionConfiguration : IEntityTypeConfiguration<Partner_Action>
  {
    void IEntityTypeConfiguration<Partner_Action>.Configure(EntityTypeBuilder<Partner_Action> builder)
    {
      builder.Property(prop => prop.Id);
      builder.Property(prop => prop.Action_Id).IsRequired();
      builder.Property(prop => prop.Partner_Id).IsRequired();
      builder.Property(prop => prop.Function).HasMaxLength(255);
    }
  }
}
