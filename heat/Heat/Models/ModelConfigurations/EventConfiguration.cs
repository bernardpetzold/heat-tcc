using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Heat.Models.ModelConfigurations
{
  public class EventConfiguration : IEntityTypeConfiguration<Event>
  {
    void IEntityTypeConfiguration<Event>.Configure(EntityTypeBuilder<Event> builder)
    {
      builder.HasKey(prop => prop.Id);
      builder.Property(prop => prop.Name).HasMaxLength(255).IsRequired();
      builder.Property(prop => prop.Customer_Id).IsRequired();
      builder.Property(prop => prop.InitDate).HasMaxLength(255);
      builder.Property(prop => prop.EndDate).HasMaxLength(255);
      builder.Property(prop => prop.IsActive).IsRequired();
    }
  }
}
