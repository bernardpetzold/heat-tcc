using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Heat.Models.ModelConfigurations
{
  public class ActionConfiguration : IEntityTypeConfiguration<Action>
  {
    void IEntityTypeConfiguration<Action>.Configure(EntityTypeBuilder<Action> builder)
    {
      builder.HasKey(prop => prop.Id);
      builder.Property(prop => prop.Name).HasMaxLength(255).IsRequired();
      builder.Property(prop => prop.Event_Id).IsRequired();
    }
  }
}
