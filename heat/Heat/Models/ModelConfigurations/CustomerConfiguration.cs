using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Heat.Models.ModelConfigurations
{
  public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
  {
    void IEntityTypeConfiguration<Customer>.Configure(EntityTypeBuilder<Customer> builder)
    {
      builder.HasKey(prop => prop.Id);
      builder.Property(prop => prop.Name).IsRequired().HasMaxLength(255);
      builder.Property(prop => prop.Contact).HasMaxLength(255);
      builder.Property(prop => prop.Phone).HasMaxLength(255);
      builder.Property(prop => prop.Email).HasMaxLength(255);
    }
  }
}
