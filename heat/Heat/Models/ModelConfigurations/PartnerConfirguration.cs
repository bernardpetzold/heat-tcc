using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Heat.Models.ModelConfigurations
{
  public class PartnerConfirguration : IEntityTypeConfiguration<Partner>
  {
    void IEntityTypeConfiguration<Partner>.Configure(EntityTypeBuilder<Partner> builder)
    {
      builder.HasKey(prop => prop.Id);
      builder.Property(prop => prop.Name).IsRequired().HasMaxLength(255);
      builder.Property(prop => prop.Phone).HasMaxLength(255);
      builder.Property(prop => prop.Email).HasMaxLength(255);
      builder.Property(prop => prop.AverageGrade).HasColumnType("Numeric");
    }
  }
}
