using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Heat.Models
{
  public class Event
  {
    public Event ()
    {
      Id = new Guid();
    }

    public Guid Id { get; set; }

    public string Name { get; set; }

    public Guid Customer_Id { get; set; }

    public string InitDate { get; set; }

    public string EndDate { get; set; }

    public bool IsActive { get; set; }
  }
}
