using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Heat.Models
{
  public class Partner
  {
    public Partner()
    {
      Id = new Guid();
    }

    [Required]
    public Guid Id { get; set; }

    [Required]
    public string Name { get; set; }

    public string Email { get; set; }

    public string Phone { get; set; }

    public int AverageGrade { get; set; }

  }
}
