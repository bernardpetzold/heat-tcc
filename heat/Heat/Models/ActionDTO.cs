using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Heat.Models
{
  public class ActionDTO
  {

    public ActionDTO()
    {

    }

    public ActionDTO(Action action, List<PartnerDTO> partners)
    {
      Id = action.Id;
      Name = action.Name;
      Partners = partners;
    }

    public Guid Id { get; set; }

    public string Name { get; set; }

    public List<PartnerDTO> Partners { get; set; }
  }
}
