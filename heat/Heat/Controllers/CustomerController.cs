using System;
using System.Linq;
using Heat.Data;
using Heat.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Heat.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class CustomerController : ControllerBase
  {

    readonly hovaneventassisstenttoolDBContext Context;

    public CustomerController(hovaneventassisstenttoolDBContext context)
    {
      Context = context;
    }

    // GET: api/Customer
    [HttpGet]
    public IActionResult Get(int? page, string order, string sortBy, string filter, string column)
    {
      try
      {
        var _customer = Context.Customers.ToList();
        int currentpage = page ?? 1;
        int numberOfEntries = 10;
        string _column = (column ?? " ").ToLower();

        if (order == "desc")
        {
          switch (sortBy)
          {
            case "name":
              _customer = Context.Customers.OrderByDescending(o => o.Name).ToList();
              break;
            case "contact":
              _customer = Context.Customers.OrderByDescending(o => o.Contact).ToList();
              break;
            default:
              _customer = Context.Customers.OrderByDescending(o => o.Id).ToList();
              break;
          }
        }
        else
        {
          switch (sortBy)
          {
            case "name":
              _customer = Context.Customers.OrderBy(o => o.Name).ToList();
              break;
            case "contact":
              _customer = Context.Customers.OrderBy(o => o.Contact).ToList();
              break;
          }
        }
        if (_column.ToLower() == "contact")
        {
          var _custList = _customer.Where(o => o.Contact.ToLower().StartsWith(filter.ToLower())).Skip((currentpage - 1) * numberOfEntries).Take(numberOfEntries).ToList();
          return Ok(_custList);
        }
        else if (_column.ToLower() == "name")
        {
          var _custList = _customer.Where(o => o.Name.ToLower().StartsWith(filter.ToLower())).Skip((currentpage - 1) * numberOfEntries).Take(numberOfEntries).ToList();
          return Ok(_custList);
        } else
        {
          var _custList = _customer.Skip((currentpage - 1) * numberOfEntries).Take(numberOfEntries).ToList();
          return Ok(_custList);
        }
      }
      catch
      {
        return StatusCode(StatusCodes.Status204NoContent);
      }
    }

    // GET: api/Customer/5
    [HttpGet("{Id}", Name = "GetCustomer")]
    public IActionResult Get(Guid id)
    {
      try
      {
        var customer = Context.Customers.Single<Customer>(o => o.Id == id);
        return Ok(customer);
      }
      catch
      {
        return NotFound();
      }
    }

    // GET: api/Customer/all
    [Route("api/[controller]/all")]
    [HttpGet("All", Name = "GetAllCustomer")]
    public IActionResult Get()
    {
      try
      {
        var customer = Context.Customers.OrderBy(o => o.Name).ToList();
        return Ok(customer);
      }
      catch
      {
        return NotFound();
      }
    }

    // POST: api/Customer
    [HttpPost]
    public IActionResult Post([FromBody] Customer _customer)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      else
      {
        var customer = new Customer();
        customer = _customer;
        Context.Add(customer);
        Context.SaveChanges();
        return StatusCode(StatusCodes.Status201Created);
      }
    }

    // PUT: api/Customer/5
    [HttpPut("{id}")]
    public IActionResult Put(Guid id, [FromBody] Customer _customer)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      else
      {
        try
        {
          _customer.Id = id;
          Context.Customers.Update(_customer);
          Context.SaveChanges();
          return Ok();
        }
        catch (Exception err)
        {
          Console.WriteLine(err);
          return BadRequest();
        }


      }
    }

    // DELETE: api/ApiWithActions/5
    [HttpDelete("{id}")]
    public IActionResult Delete(Guid id)
    {
      try
      {
        var customer = Context.Customers.Single<Customer>(o => o.Id == id);
        Context.Customers.Remove(customer);
        Context.SaveChanges();
        return Ok();
      }
      catch
      {
        return StatusCode(StatusCodes.Status404NotFound);
      }
    }
  }
}
