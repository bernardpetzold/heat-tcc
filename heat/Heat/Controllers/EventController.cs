using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Heat.Data;
using Heat.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Heat.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class EventController : ControllerBase
  {
    readonly hovaneventassisstenttoolDBContext Context;

    public EventController(hovaneventassisstenttoolDBContext context)
    {
      Context = context;
    }

    // GET: api/Event
    [HttpGet]
    public IActionResult Get(int? page, string filter, string column)
    {
      try
      {
        var _events = Context.Events.ToList();
        var events = new List<EventDTO>();
        int currentpage = page ?? 1;
        int numberOfEntries = 10;
        string _column = (column ?? " ").ToLower();

        switch (_column)
        {
          case "name":
            _events = _events.Where(o => o.Name.ToLower().StartsWith(filter.ToLower())).Skip((currentpage - 1) * numberOfEntries).Take(numberOfEntries).ToList();
            break;
          case "customer":
            _events = _events.Where(o => o.Customer_Id.Equals(new Guid(filter))).Skip((currentpage - 1) * numberOfEntries).Take(numberOfEntries).ToList();
            break;
          default:
            _events = _events.Skip((currentpage - 1) * numberOfEntries).Take(numberOfEntries).ToList();
            break;
        }

        foreach (Event element in _events)
        {
          events.Add(new EventDTO(element, Context.Customers.Single(o => o.Id == element.Customer_Id)));
        }
        return Ok(events);
      }
      catch (InvalidCastException err)
      {
        return BadRequest(err);
      }
    }

    // GET: api/Event/5
    [HttpGet("{id}", Name = "Get")]
    public IActionResult Get(Guid id)
    {
      try
      {
        var _event = Context.Events.Single(o => o.Id == id);
        var _customer = Context.Customers.Single(o => o.Id == _event.Customer_Id);
        var _actions = Context.Actions.Where(o => o.Event_Id == _event.Id).ToList();
        var actions = new List<ActionDTO>();
        foreach (Models.Action element in _actions)
        {
          var _partner = new List<PartnerDTO>();
          var _partneractions = Context.Partner_Action.Where(o => o.Action_Id == element.Id).ToList();
          foreach (Partner_Action element2 in _partneractions)
          {
            _partner.Add(new PartnerDTO(Context.Partners.Single(o => o.Id == element2.Partner_Id), element2.Function));
          }
          actions.Add(new ActionDTO(element, _partner));
        }
        var response = new EventDTO(_event, _customer, actions);
        return Ok(response);
      }
      catch (InvalidCastException err)
      {
        return BadRequest(err);
      }

    }

    // POST: api/Event
    [HttpPost]
    public IActionResult Post([FromBody] EventDTO _eventDTO)
    {
      try
      {
        var _event = new Event();
        _event.Name = _eventDTO.Name;
        _event.Customer_Id = _eventDTO.Customer_Id;
        _event.InitDate = _eventDTO.InitDate;
        _event.EndDate = _eventDTO.EndDate;
        _event.IsActive = true;
        Context.Events.Add(_event);
        Context.SaveChanges();
        foreach (ActionDTO element in _eventDTO.Actions)
        {
          var _action = new Models.Action();
          _action.Name = element.Name;
          _action.Event_Id = _event.Id;
          Context.Actions.Add(_action);
          foreach (PartnerDTO element2 in element.Partners)
          {
            var _partnerAction = new Partner_Action();
            _partnerAction.Action_Id = _action.Id;
            _partnerAction.Partner_Id = element2.Id;
            _partnerAction.Function = element2.Function;
            Context.Partner_Action.Add(_partnerAction);
          }
        }
        Context.SaveChanges();
        return Ok();
      }
      catch (InvalidCastException err)
      {
        return BadRequest(err);
      }
    }

    // PUT: api/Event/5
    [HttpPut("{id}", Name = "PutEvent")]
    public IActionResult Put(Guid id, [FromBody] EventDTO _eventDTO)
    {
      var _event = Context.Events.Single(o => o.Id == id);
      _event.Name = _eventDTO.Name;
      _event.Customer_Id = _eventDTO.Customer_Id;
      _event.InitDate = _eventDTO.InitDate;
      _event.EndDate = _eventDTO.EndDate;
      _event.IsActive = _eventDTO.IsActive;
      Context.Events.Update(_event);

      var actions = Context.Actions.Where(o => o.Event_Id == id).ToList();
      foreach (Models.Action element in actions)
      {
        var aux = false;
        foreach (ActionDTO element2 in _eventDTO.Actions)
        {
          if (element.Id == element2.Id)
          {
            aux = true;
            var actualPAs = Context.Partner_Action.Where(o => o.Action_Id == element.Id).ToList();
            foreach( Partner_Action aPAs in actualPAs)
            {
              var aux2 = false;
              foreach (PartnerDTO partner in element2.Partners)
              {
                if (aPAs.Partner_Id == partner.Id)
                {
                  aux2 = true;
                }
              }
              if (aux2 == false)
              {
                Context.Partner_Action.Remove(aPAs);
                Context.SaveChanges();
                Context.Entry(aPAs).State = Microsoft.EntityFrameworkCore.EntityState.Detached;
              }
            }
          }
        }
        if (aux == false)
        {
          var pas = Context.Partner_Action.Where(o => o.Action_Id == element.Id).ToList();
          foreach (Partner_Action pa in pas)
          {
            Context.Partner_Action.Remove(pa);
          }
          Context.Actions.Remove(element);
          Context.SaveChanges();
        }
        Context.Entry(element).State = Microsoft.EntityFrameworkCore.EntityState.Detached;
      }

      foreach (ActionDTO element in _eventDTO.Actions)
      {
        var _action = new Models.Action();
        _action.Name = element.Name;
        _action.Event_Id = _event.Id;
        if (element.Id.ToString() == "00000000-0000-0000-0000-000000000000")
        {
          Context.Actions.AddAsync(_action);
        }
        else
        {
          _action.Id = element.Id;
          Context.Actions.Update(_action);
          foreach (PartnerDTO element2 in element.Partners)
          {
            if (Context.Partner_Action.Any(o => o.Action_Id == _action.Id && o.Partner_Id == element2.Id))
            {
              Context.Partner_Action.Update(Context.Partner_Action.Where(o => o.Action_Id == _action.Id && o.Partner_Id == element2.Id).Single());
            }
            else
            {
              var _partnerAction = new Partner_Action();
              _partnerAction.Action_Id = _action.Id;
              _partnerAction.Partner_Id = element2.Id;
              _partnerAction.Function = element2.Function;
              Context.Partner_Action.Add(_partnerAction);
            }
          }
        }
      }
      Context.SaveChanges();
      return Ok();

    }

    // DELETE: api/ApiWithActions/5
    [HttpDelete("{id}")]
    public IActionResult Delete(Guid Id)
    {
      try
      {
        var _event = Context.Events.Single(o => o.Id == Id);
        var _actions = Context.Actions.Where(o => o.Event_Id == Id).ToList();
        foreach (Models.Action element in _actions)
        {
          var _partner_action = Context.Partner_Action.Where(o => o.Action_Id == element.Id);
          foreach (Partner_Action element2 in _partner_action)
          {
            Context.Partner_Action.Remove(element2);
          }
          Context.Actions.Remove(element);
        }
        Context.Events.Remove(_event);
        Context.SaveChanges();
        return Ok();
      }
      catch (InvalidCastException err)
      {
        return BadRequest(err);
      }
    }
  }
}
