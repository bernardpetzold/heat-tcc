using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Heat.Data;
using Heat.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Heat.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class PartnerController : ControllerBase
  {

    readonly hovaneventassisstenttoolDBContext Context;

    public PartnerController(hovaneventassisstenttoolDBContext context)
    {
      Context = context;
    }

    // GET: api/Partner
    [HttpGet]
    public IActionResult Get(int? page, string order, string sortBy, string filter)
    {
      try
      {
      var _partner = Context.Partners.ToList();
      int currentpage = page ?? 1;
      int numberOfEntries = 10;
        if (order == "desc")
        {
          switch (sortBy)
          {
            case "name":
              _partner = Context.Partners.OrderByDescending(o => o.Name).ToList();
              break;
            default:
              _partner = Context.Partners.OrderByDescending(o => o.Id).ToList();
              break;
          }
        }
        else
        {
          if (sortBy == "name")
          {
            _partner = Context.Partners.OrderBy(o => o.Name).ToList();
          }
        }
        if (filter != null)
        {
          var _custList = _partner.Where(o => o.Name.ToLower().StartsWith(filter.ToLower())).Skip((currentpage - 1) * numberOfEntries).Take(numberOfEntries).ToList();
          return Ok(_custList);
        }
        else
        {
          var _custList = _partner.Skip((currentpage - 1) * numberOfEntries).Take(numberOfEntries).ToList();
          return Ok(_custList);
        }
      }
      catch
      {
        return StatusCode(StatusCodes.Status204NoContent);
      }
    }

    // GET: api/Partner/5
    [HttpGet("{id}", Name = "GetPartner")]
    public IActionResult Get(Guid id)
    {
      try
      {
        var partner = Context.Partners.Single<Partner>(o => o.Id == id);
        return Ok(partner);
      }
      catch
      {
        return NotFound();
      };
    }

    // GET: api/Partner/all
    [Route("api/[controller]/all")]
    [HttpGet("All", Name = "GetAllPartner")]
    public IActionResult Get()
    {
      try
      {
        var partner = Context.Partners.OrderBy(o => o.Name).ToList();
        return Ok(partner);
      }
      catch
      {
        return NotFound();
      };
    }

    // POST: api/Partner
    [HttpPost]
    public IActionResult Post([FromBody] Partner _partner)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      else
      {
        var partner = new Partner();
        partner = _partner;
        Context.Add(partner);
        Context.SaveChanges();
        return StatusCode(StatusCodes.Status201Created);
      }
    }

    // PUT: api/Partner/5
    [HttpPut("{id}")]
    public IActionResult Put(Guid id, [FromBody] Partner _partner)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      else
      {
        try
        {
          _partner.Id = id;
          Context.Partners.Update(_partner);
          Context.SaveChanges();
          return Ok();
        }
        catch (Exception err)
        {
          Console.WriteLine(err);
          return BadRequest();
        }


      }
    }

    // DELETE: api/ApiWithActions/5
    [HttpDelete("{id}")]
    public IActionResult Delete(Guid id)
    {
      try
      {
        var partner = Context.Partners.Single<Partner>(o => o.Id == id);
        Context.Partners.Remove(partner);
        Context.SaveChanges();
        return Ok();
      }
      catch
      {
        return StatusCode(StatusCodes.Status404NotFound);
      }
    }
  }
}
