using Heat.Models;
using Heat.Models.ModelConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Heat.Data
{
  public class hovaneventassisstenttoolDBContext : DbContext
  {
    public hovaneventassisstenttoolDBContext(DbContextOptions<hovaneventassisstenttoolDBContext> options) : base(options) { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.ApplyConfiguration(new CustomerConfiguration());
      modelBuilder.ApplyConfiguration(new PartnerConfirguration());
    }

    public DbSet<Customer> Customers { get; set; }

    public DbSet<Partner> Partners { get; set; }

    public DbSet<Event> Events { get; set; }

    public DbSet<Action> Actions { get; set; }

    public DbSet<Partner_Action> Partner_Action { get; set; }
  }
}
