using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Heat.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Heat
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddControllers();
      var builder = new PostgreSqlConnectionStringBuilder("postgres://rvzkugacjxqocu:d0c7bd2f6d12bf4815b1c99a150e3644cb629e21a3719576ef848ad267f98ee7@ec2-174-129-27-158.compute-1.amazonaws.com:5432/d5ju74lbtg93ve")
      {
        Pooling = true,
        TrustServerCertificate = true,
        SslMode = SslMode.Require
      };
      services.AddDbContext<hovaneventassisstenttoolDBContext>(
        options => options.UseNpgsql(
          builder.ConnectionString
          )
        );
      services.AddCors(options =>
      {
        options.AddPolicy(MyAllowSpecificOrigins,
        builder =>
        {
          builder.WithOrigins("*");
          builder.WithHeaders("*");
          builder.AllowAnyMethod();
        });
      });
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      app.UseCors(MyAllowSpecificOrigins);

      app.UseHttpsRedirection();

      app.UseRouting();

      app.UseAuthorization();

      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllers();
      });
    }
  }
}
